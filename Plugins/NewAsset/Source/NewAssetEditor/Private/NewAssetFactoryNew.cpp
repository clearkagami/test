﻿#pragma once

#include "NewAssetFactoryNew.h"
#include "NewAssetClass.h"
#include "AssetTypeCategories.h"

UNewAssetFactoryNew::UNewAssetFactoryNew()
{
	SupportedClass = UNewAsset::StaticClass();
	bCreateNew = true;
	bEditAfterNew = true;
}

UObject* UNewAssetFactoryNew::FactoryCreateNew(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags,
	UObject* Context, FFeedbackContext* Warn)
{
	return NewObject<UNewAsset>(InParent, InClass, InName, Flags);
}

bool UNewAssetFactoryNew::ShouldShowInNewMenu() const
{
	return true;
}
