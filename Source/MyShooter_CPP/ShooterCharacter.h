// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Sound/SoundCue.h"
#include "AmmoType.h"

#include "ShooterCharacter.generated.h"


UENUM(BlueprintType)
enum class ECombatState : uint8
{
	ECS_Unoccupied UMETA(DisplayName = "Unoccupied"),
	ECS_FireTimerInProgress UMETA(DisplayName = "FireTimerInProgress"),
	ECS_Reloading UMETA(DisplayName = "Reloading"),

	ECS_MAX UMETA(DisplayName = "DefaultMAX"),
};

UCLASS()
class MYSHOOTER_CPP_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

/*--------------------------------------------------------------------------------------------------------------------*/	
	/*Called per frame for forwards/backwards input*/
	void MoveForward(float Value);

	/*Called per frame for side to side input*/
	void MoveRight(float Value);

	/*Called per frame for turn input*/
	void Turn(float Value);

	/*Called per frame for look up/down input*/
	void LookUp(float Value);

	void Fire();

	/*Determine final hit location of Fire by applying line trace two times, then return if has a hit*/
	bool DetermineFireHitLocation(const FVector& SocketLocation, FVector& HitLocation);

	/*Set bAiming to true or false with button press*/
	void AimingButtonPressed();
	void AimingButtonReleased();

	void CameraInterpZoom(float DeltaTime);

	/*Set BaseTurnRate and BaseLookUpRate based on aiming*/
	void SetLookRates();

	/*Calculate CrossHairSpreadMultiplier by several factors*/
	void CalculateCrossHairSpread(float DeltaTime);

	void StartCrossHairBulletFire();
	
	UFUNCTION()
	void FinishCrossHairBulletFire();

	void FireButtonPressed();
	void FireButtonReleased();

	void StartFireTimer();

	UFUNCTION()
	void AutoFireReset();

	/*Line trace for items under the cross hairs*/
	bool TraceUnderCrossHairs(FHitResult& OutHitResult, float TraceLength);

	/*Trace for items if OverlappedItemCount > 0*/
	void TraceForItems();

	/*Spawns a default weapon and eqiups it*/
	class AWeapon* SpawnDefaultWeapon();

	/*Takes a weapon and attaches it to the mesh*/
	void EquipWeapon(class AWeapon* WeaponToEquip);

	/*Detach weapon and let it fall to the ground*/
	void DropWeapon();

	void SelectButtonPressed();
	void SelectButtonReleased();

	/*Drops currently equipped Weapon and Equips TraceHitItem*/
	void SwapWeapon(AWeapon* WeaponToSwap);

	/*Initialize the Ammo Map with ammo values*/
	void InitializeAmmoMap();

	/*Check to make sure our weapon has ammo*/
	bool EquippedWeaponHasAmmo();
	
	/*Fire functions*/
	void PlayFireSound();
	void SendBullet();
	void PlayGunFireMontage();

	void ReloadButtonPressed();

	void Reload();

	UFUNCTION(BlueprintCallable)
	void FinishingReloading();

	/*Checks to see if we have ammo of the EquippedWeapon's ammo type*/
	bool QueryCarryingAmmo();

	/*Called from Animation Blueprint with Grab Clip notify*/
	UFUNCTION(BlueprintCallable)
	void GrabClip();

	/*Called from Animation Blueprint with Release Clip notify*/
	UFUNCTION(BlueprintCallable)
	void ReleaseClip();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;



/*--------------------------------------------------------------------------------------------------------------------*/
	private:
	/*Declare CameraBoom positioning the camera behind the character*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	/*Camera that follows the character*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	/*Base turn rate in degrees/s, Other scale may affect final turn rate*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float BaseTurnRate;

	/*Base look up/down rate in degrees/s, Other scale may affect final turn rate*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float BaseLookUpRate;

	/*Turn rate while not aiming*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float HipTurnRate;

	/*Look up rate when not aiming*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float HipLookUpRate;

	/*Turn rate when aiming*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float AimingTurnRate;

	/*Look up rate when aiming*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float AimingLookUpRate;

	/*Randomized gunshot sound*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	USoundCue* FireSound;

	/*Flash spawned at skeletal mesh socket called "BarrelSocket"*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UParticleSystem* MuzzleFlash;
	
	/*Montage for firing the weapon*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* HipFireMontage;
	
	/*FX at fire hit point */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UParticleSystem* FireHitParticle;

	/*Smoke trail for bullets */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UParticleSystem* BeamParticle;

	/*True when aiming*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	bool bAiming;

	/*Default camera field of view value*/
	float CameraDefaultFOV;

	/*Field of view value for when zoomed in*/
	float CameraZoomedFOV;

	/*Current field of view this frame*/
	float CameraCurrentFOV;

	/*Interp speed for zooming when aiming*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float ZoomInterpSpeed;

	/*Cross Hair Spread*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CrossHairs, meta = (AllowPrivateAccess = "true"))
	float CrossHairSpreadMultiplier;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CrossHairs, meta = (AllowPrivateAccess = "true"))
	float CrossHairVelocityFactor;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CrossHairs, meta = (AllowPrivateAccess = "true"))
	float CrossHairInAirFactor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CrossHairs, meta = (AllowPrivateAccess = "true"))
	float CrossHairAimFactor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CrossHairs, meta = (AllowPrivateAccess = "true"))
	float CrossHairShootingFactor;

	/*Used for function StartCrossHairBulletFire*/
	float ShootTimeDuration;
	bool bFiringBullet;
	FTimerHandle CrossHairShootTimer;

	/*Left mouse button or right console trigger pressed*/
	bool bFireButtonPressed;

	/*True when we can fire. False when waiting for the timer*/
	bool bShouldFire;

	/*Rate of automatic gun fire*/
	float AutomaticFireRate;

	/*Sets a timer between gunshots*/
	FTimerHandle AutoFireTimer;

	/*True if we should trace every frame for items*/
	bool bShouldTraceForItems;

	/*Length of the trace items*/
	float ItemTraceLength;

	/*Number of overlapped items*/
	int8 OverlappedItemCount;

	/*The AItem we hit last frame*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	class AItem* TraceHitItemLastFrame;
	
	/*Currently equipped Weapon*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class AWeapon* EquippedWeapon;

	/*Set this in Blueprints for the default Weapon class*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AWeapon> DefaultWeaponClass;

	/*The Item currently hit by our trace in TraceForItems (could be null)*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	AItem* TraceHitItem;

	/*Distance outward from the camera for the interp destination*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	float CameraInterpDistance;

	/*Distance upward from the camera for the interp destination*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	float CameraInterpElevation;

	/*Map to keep track of ammo of the different ammo types*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	TMap<EAmmoType, int32> AmmoMap;

	/*Starting amount of 9mm ammo*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Items, meta = (AllowPrivateAccess = "true"))
	int32 Starting9mmAmmo;

	/*Starting amount of AR ammo*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Items, meta = (AllowPrivateAccess = "true"))
	int32 StartingARAmmo;

	/*Combat State, can only fire or reload if Unoccupied*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	ECombatState CombatState;

	/*Montage for reload animations*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ReloadMontage;

	/*Transform of the clip when we first grab the clip during reloading*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FTransform ClipTransform;

	/*Scene component to attach to the Character's hand during reloading*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	USceneComponent* HandSceneComponent;



	
	public:
	/*Returns CameraBoom subobject*/
	FORCEINLINE USpringArmComponent* GetCameraBoom() const {return CameraBoom;}

	/*Returns FollowCamera subobject*/
	FORCEINLINE UCameraComponent* GetFollowCamera() const {return FollowCamera;}

	FORCEINLINE bool GetAiming() const {return bAiming;}

	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetCrossHairSpreadMultiplier() const;

	FORCEINLINE	int8 GetOverlappedItemCount() const {return OverlappedItemCount;}

	/*Adds/subtracts to/from OverlappedItemCount and updates bShouldTraceForItems*/
	void IncrementOverlappedItemCount(int8 Amount);

	FVector GetCameraInterpLocation();

	void GetPickupItem(AItem* Item);

	FORCEINLINE ECombatState GetCombatState() const {return CombatState;} 
};
