﻿#pragma once
#include "EditorReimportHandler.h"

#include "NewAssetFactoryFile.generated.h"

UCLASS()
class UNewAssetFactoryFile : public UFactory, public FReimportHandler
{
	GENERATED_BODY()

public:
	UNewAssetFactoryFile();
	
	virtual UObject* FactoryCreateFile(
		UClass* InClass,
		UObject* InParent,
		FName InName,
		EObjectFlags Flags,
		const FString& Filename,
		const TCHAR* Parms,
		FFeedbackContext* Warn,
		bool& bOutOperationCanceled
		) override;

	virtual bool CanReimport(UObject* Obj, TArray<FString>& OutFilenames) override;
	virtual void SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths) override;;
	virtual EReimportResult::Type Reimport(UObject* Obj) override;
};