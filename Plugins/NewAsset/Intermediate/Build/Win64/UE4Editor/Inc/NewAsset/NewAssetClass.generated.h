// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NEWASSET_NewAssetClass_generated_h
#error "NewAssetClass.generated.h already included, missing '#pragma once' in NewAssetClass.h"
#endif
#define NEWASSET_NewAssetClass_generated_h

#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_SPARSE_DATA
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInc);


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInc);


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNewAsset(); \
	friend struct Z_Construct_UClass_UNewAsset_Statics; \
public: \
	DECLARE_CLASS(UNewAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NewAsset"), NO_API) \
	DECLARE_SERIALIZER(UNewAsset)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUNewAsset(); \
	friend struct Z_Construct_UClass_UNewAsset_Statics; \
public: \
	DECLARE_CLASS(UNewAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NewAsset"), NO_API) \
	DECLARE_SERIALIZER(UNewAsset)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNewAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAsset(UNewAsset&&); \
	NO_API UNewAsset(const UNewAsset&); \
public:


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNewAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAsset(UNewAsset&&); \
	NO_API UNewAsset(const UNewAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAsset); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewAsset)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_7_PROLOG
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_INCLASS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NEWASSET_API UClass* StaticClass<class UNewAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_NewAsset_Source_NewAsset_Public_NewAssetClass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
