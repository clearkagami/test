// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_ThirdPartyLibrary.h"
#include "Core.h"
#include "ThirdLibInvoker.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"
#include "ThirdParty/PI_ThirdPartyLibraryLibrary/ExampleLibrary.h"

#define LOCTEXT_NAMESPACE "FPI_ThirdPartyLibraryModule"

void FPI_ThirdPartyLibraryModule::StartupModule()
{
	Lib = NewObject<UThirdLibInvoker>();
	Lib->InvokeLib();
}

void FPI_ThirdPartyLibraryModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPI_ThirdPartyLibraryModule, PI_ThirdPartyLibrary)
