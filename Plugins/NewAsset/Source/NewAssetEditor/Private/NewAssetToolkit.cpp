﻿#include "NewAssetToolkit.h"

#include "NewAssetClass.h"
#include "Widgets/Input/SSlider.h"


namespace NewAssetToolkit
{
	static const FName AppIdentifier("NewAssetEditorApp");
	static const FName TabId("NewAssetEditorTab");
}


void FNewAssetToolkit::RegisterTabSpawners(const TSharedRef<FTabManager>& InTabManager)
{
	FAssetEditorToolkit::RegisterTabSpawners(InTabManager);
	SAssignNew(TextBlock, STextBlock).Text(FText::FromString(FString::FromInt(IsValid(NewAsset) ? NewAsset->IntValue : 0)));

	InTabManager->RegisterTabSpawner(
		NewAssetToolkit::TabId,
		FOnSpawnTab::CreateRaw(this, &FNewAssetToolkit::OnSpawnNewAssetToolkit)
		);
	
}

void FNewAssetToolkit::UnregisterTabSpawners(const TSharedRef<FTabManager>& InTabManager)
{
	FAssetEditorToolkit::UnregisterTabSpawners(InTabManager);

	InTabManager->UnregisterTabSpawner(NewAssetToolkit::TabId);
}

void FNewAssetToolkit::Initialize(UNewAsset* InNewAsset, const EToolkitMode::Type InMode,
	TSharedPtr<IToolkitHost> EditWithinLevelEditor)
{
	NewAsset = InNewAsset;
	const TSharedRef<FTabManager::FLayout> Layout
	= FTabManager::NewLayout("NewAssetEditorLayout")->AddArea(
		FTabManager::NewPrimaryArea()
		->SetOrientation(Orient_Horizontal)
		->Split(
			FTabManager::NewSplitter()
				->SetOrientation(Orient_Vertical)
				->Split(
					FTabManager::NewStack()
					->AddTab(GetToolbarTabId(), ETabState::OpenedTab)
					)
				->Split(
					FTabManager::NewStack()
					->AddTab(NewAssetToolkit::TabId, ETabState::OpenedTab)
					)	
			)
		);
	InitAssetEditor(InMode, EditWithinLevelEditor, NewAssetToolkit::AppIdentifier, Layout, true, true, InNewAsset);
	RegenerateMenusAndToolbars();
}

FText FNewAssetToolkit::GetBaseToolkitName() const
{
	return FText::FromString(TEXT("NewAssetBaseToolkit"));
}

FName FNewAssetToolkit::GetToolkitFName() const
{
	return FName("NewAssetToolkit");
}

FLinearColor FNewAssetToolkit::GetWorldCentricTabColorScale() const
{
	return FLinearColor(0.5f, 0.2f, 0.3f, 0.4f);
}

FString FNewAssetToolkit::GetWorldCentricTabPrefix() const
{
	return FString(TEXT("NewAssetEditorPrefix"));
}

TSharedRef<SDockTab> FNewAssetToolkit::OnSpawnNewAssetToolkit(const FSpawnTabArgs& SpawnTabArgs)
{
	FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	FDetailsViewArgs DetailsViewArgs;
	DetailsView = PropertyEditorModule.CreateDetailView(DetailsViewArgs);
	DetailsView->SetObject(NewAsset);



	return SNew(SDockTab)
		.TabRole(ETabRole::PanelTab)
		[
			SNew(SHorizontalBox)
			+SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Fill).VAlign(VAlign_Top)
			[
				DetailsView.ToSharedRef()
			]
			+SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				TextBlock.ToSharedRef()
			]
			+SHorizontalBox::Slot().FillWidth(2).HAlign(HAlign_Fill).VAlign(VAlign_Center)
			[
				SNew(SSlider)
				.Value(IsValid(NewAsset) ? NewAsset->IntValue / 10000.f : 0)
				.OnValueChanged_Lambda(
					[this](float value)
					{
						if(TextBlock.IsValid())
						{
							TextBlock->SetText(FText::FromString(FString::FromInt((int32)(value * 10000))));
						}

						if(IsValid(NewAsset))
						{
							NewAsset->IntValue = (int32)(value * 10000);
						}
					}
					)
				.OnMouseCaptureEnd_Lambda(
					[this]()
					{
						NewAsset->MarkPackageDirty();
					}
					)	
			]
		];
}
