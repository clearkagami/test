// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NEWASSETEDITOR_NewAssetFactoryFile_generated_h
#error "NewAssetFactoryFile.generated.h already included, missing '#pragma once' in NewAssetFactoryFile.h"
#endif
#define NEWASSETEDITOR_NewAssetFactoryFile_generated_h

#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_SPARSE_DATA
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_RPC_WRAPPERS
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_RPC_WRAPPERS_NO_PURE_DECLS
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNewAssetFactoryFile(); \
	friend struct Z_Construct_UClass_UNewAssetFactoryFile_Statics; \
public: \
	DECLARE_CLASS(UNewAssetFactoryFile, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NewAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UNewAssetFactoryFile)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_INCLASS \
private: \
	static void StaticRegisterNativesUNewAssetFactoryFile(); \
	friend struct Z_Construct_UClass_UNewAssetFactoryFile_Statics; \
public: \
	DECLARE_CLASS(UNewAssetFactoryFile, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NewAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UNewAssetFactoryFile)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNewAssetFactoryFile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewAssetFactoryFile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAssetFactoryFile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAssetFactoryFile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAssetFactoryFile(UNewAssetFactoryFile&&); \
	NO_API UNewAssetFactoryFile(const UNewAssetFactoryFile&); \
public:


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAssetFactoryFile(UNewAssetFactoryFile&&); \
	NO_API UNewAssetFactoryFile(const UNewAssetFactoryFile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAssetFactoryFile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAssetFactoryFile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNewAssetFactoryFile)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_6_PROLOG
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_INCLASS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NEWASSETEDITOR_API UClass* StaticClass<class UNewAssetFactoryFile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryFile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
