﻿#include "CoreMinimal.h"
#include "Exporters/Exporter.h"

#include "NewAssetExporter.generated.h"

UCLASS()
class UNewAssetExporter : public UExporter
{
	GENERATED_BODY()

	UNewAssetExporter();
	
	virtual bool SupportsObject(UObject* Object) const override;
	virtual bool ExportBinary(UObject* Object, const TCHAR* Type, FArchive& Ar, FFeedbackContext* Warn, int32 FileIndex, uint32 PortFlags) override;
};


