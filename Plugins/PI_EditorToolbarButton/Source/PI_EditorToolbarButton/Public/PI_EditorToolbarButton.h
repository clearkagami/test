// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "BlueprintEditor.h"

class FToolBarBuilder;
class FMenuBuilder;

class FPI_EditorToolbarButtonModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	
	/** This function will be bound to Command. */
	void PluginButtonClicked();
	
private:

	void RegisterMenus();

	void AddMenuExtension(FMenuBuilder& Builder);

	void AddMenuBarExtension(FMenuBarBuilder& Builder);

	void AddToolBarExtension(FToolBarBuilder& Builder);

	void FuncOnBlueprintToolBarRegister(
		FWorkflowAllowedTabSet& WorkflowAllowedTabSet,
		FName Name,
		TSharedPtr<FBlueprintEditor> BlueprintEditor
		);

private:
	TSharedPtr<class FUICommandList> PluginCommands;
};
