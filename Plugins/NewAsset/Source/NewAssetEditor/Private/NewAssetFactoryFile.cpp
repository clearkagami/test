﻿#include "NewAssetFactoryFile.h"
#include "NewAssetClass.h"
#include "Misc/FileHelper.h"

UNewAssetFactoryFile::UNewAssetFactoryFile()
{
	Formats.Add(FString(TEXT("myfile;")));
	SupportedClass = UNewAsset::StaticClass();
	bCreateNew = false;
	bEditorImport = true;
}

UObject* UNewAssetFactoryFile::FactoryCreateFile(UClass* InClass, UObject* InParent, FName InName, EObjectFlags Flags,
                                                 const FString& Filename, const TCHAR* Parms, FFeedbackContext* Warn, bool& bOutOperationCanceled)
{
	UNewAsset* NewAsset = nullptr;
	TArray<uint8> Bytes;

	if(FFileHelper::LoadFileToArray(Bytes, *Filename) && Bytes.Num() >= sizeof(int32))
	{
		NewAsset = NewObject<UNewAsset>(InParent, InClass, InName, Flags);
		for(uint32 i = 0; i < sizeof(int32); ++i)
		{
			NewAsset->IntValue |= Bytes[i] << (i * 8);
		}
		
	}

	bOutOperationCanceled = false;

	return NewAsset;
}

bool UNewAssetFactoryFile::CanReimport(UObject* Obj, TArray<FString>& OutFilenames)
{
	return true;
}

void UNewAssetFactoryFile::SetReimportPaths(UObject* Obj, const TArray<FString>& NewReimportPaths)
{
}

EReimportResult::Type UNewAssetFactoryFile::Reimport(UObject* Obj)
{
	if(IsValid(Obj))
	{
		bool OutCancel = false;
		UObject* NewObj = ImportObject(
			Obj->GetClass(),
			Obj->GetOuter(),
			Obj->GetFName(),
			Obj->GetFlags(),
			PreferredReimportPath,
			nullptr,
			OutCancel	
			);
		if(!OutCancel)
		{
			UNewAsset* NewAsset = Cast<UNewAsset>(NewObj);
			return EReimportResult::Succeeded;
		}
	}
	return EReimportResult::Failed;
}
