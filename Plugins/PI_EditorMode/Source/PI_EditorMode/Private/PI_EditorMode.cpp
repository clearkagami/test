// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_EditorMode.h"
#include "PI_EditorModeEdMode.h"

#define LOCTEXT_NAMESPACE "FPI_EditorModeModule"

void FPI_EditorModeModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	FEditorModeRegistry::Get().RegisterMode<FPI_EditorModeEdMode>(FPI_EditorModeEdMode::EM_PI_EditorModeEdModeId, LOCTEXT("PI_EditorModeEdModeName", "PI_EditorModeEdMode"), FSlateIcon(), true);
}

void FPI_EditorModeModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FEditorModeRegistry::Get().UnregisterMode(FPI_EditorModeEdMode::EM_PI_EditorModeEdModeId);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPI_EditorModeModule, PI_EditorMode)