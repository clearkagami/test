﻿#include "NewAssetDetailCustomization.h"

#include "DetailCategoryBuilder.h"
#include "DetailLayoutBuilder.h"
#include "DetailWidgetRow.h"
#include "NewAssetClass.h"
#include "PropertyCustomizationHelpers.h"


void FNewAssetDetailCustomization::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	IDetailCategoryBuilder& DetailCategoryBuilderInstance = DetailBuilder.EditCategory(TEXT("CustomCategory"));
	TSharedPtr<IPropertyHandle> NewAssetIntValueProperty = DetailBuilder.GetProperty(GET_MEMBER_NAME_CHECKED(UNewAsset, IntValue));
	DetailBuilder.AddPropertyToCategory(NewAssetIntValueProperty);
	DetailCategoryBuilderInstance
		.AddCustomRow(FText::FromString(TEXT("NewAssetCustomRow")))
		.WholeRowContent()
		[
			SNew(SProperty, NewAssetIntValueProperty).ShouldDisplayName(false).CustomWidget()
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Fill).VAlign(VAlign_Center)
				[
					NewAssetIntValueProperty->CreatePropertyNameWidget()
				]
				+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Fill).VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Text_Lambda(
						[NewAssetIntValueProperty]
						{
							int32 Value;
							NewAssetIntValueProperty->GetValue(Value);
							return FText::FromString(FString::FromInt(Value));
						}
						)
				]
			]
		
		];
}
