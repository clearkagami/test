// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NEWASSETEDITOR_NewAssetExporter_generated_h
#error "NewAssetExporter.generated.h already included, missing '#pragma once' in NewAssetExporter.h"
#endif
#define NEWASSETEDITOR_NewAssetExporter_generated_h

#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_SPARSE_DATA
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_RPC_WRAPPERS
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_RPC_WRAPPERS_NO_PURE_DECLS
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNewAssetExporter(); \
	friend struct Z_Construct_UClass_UNewAssetExporter_Statics; \
public: \
	DECLARE_CLASS(UNewAssetExporter, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/NewAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UNewAssetExporter)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_INCLASS \
private: \
	static void StaticRegisterNativesUNewAssetExporter(); \
	friend struct Z_Construct_UClass_UNewAssetExporter_Statics; \
public: \
	DECLARE_CLASS(UNewAssetExporter, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/NewAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UNewAssetExporter)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNewAssetExporter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewAssetExporter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAssetExporter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAssetExporter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAssetExporter(UNewAssetExporter&&); \
	NO_API UNewAssetExporter(const UNewAssetExporter&); \
public:


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAssetExporter(UNewAssetExporter&&); \
	NO_API UNewAssetExporter(const UNewAssetExporter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAssetExporter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAssetExporter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNewAssetExporter)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_6_PROLOG
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_INCLASS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NEWASSETEDITOR_API UClass* StaticClass<class UNewAssetExporter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetExporter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
