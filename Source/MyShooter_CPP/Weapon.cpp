// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

AWeapon::AWeapon():
	ThrowWeaponTime(0.7f),
	bFalling(false),
	MagazineCapacity(30),
	Ammo(MagazineCapacity),
	WeaponType(EWeaponType::EWT_SubmachineGun),
	AmmoType(EAmmoType::EAT_9mm),
	ReloadMontageSection(FName(TEXT("Reload SMG"))),
	ClipBoneName(TEXT("smg_clip"))
{
	PrimaryActorTick.bCanEverTick = true;
}

void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*Keep the Weapon upright*/
	if(GetItemState() == EItemState::EIS_Falling && bFalling)
	{
		const FRotator MeshRotation{0.f, GetRootComponent()->GetComponentRotation().Yaw, 0.f};
		GetRootComponent()->SetWorldRotation(
			MeshRotation,
			false,
			nullptr,
			ETeleportType::TeleportPhysics);
	}
}

void AWeapon::WeaponBeThrown()
{
	/*Adjust rotation to upright so ForwardVector and RightVector below is correct*/
	FRotator MeshRotation{0.f, GetRootComponent()->GetComponentRotation().Yaw, 0.f};
	GetRootComponent()->SetWorldRotation(
		MeshRotation,
		false,
		nullptr,
		ETeleportType::TeleportPhysics);

	/*Add an impulse*/
	const FVector MeshForward{GetRootComponent()->GetForwardVector()};
	const FVector MeshRight{GetRootComponent()->GetRightVector()};
	FVector ImpulseVector = MeshRight.RotateAngleAxis(-20.f, MeshForward);

	/*if(GEngine)
	{
		FString DebugMessage{FString::Printf(TEXT("Right:%f %f %f"), MeshRight.X, MeshRight.Y, MeshRight.Z)};
		GEngine->AddOnScreenDebugMessage(1, 10.f, FColor::Blue, DebugMessage);
		DebugMessage = {FString::Printf(TEXT("Forward:%f %f %f"), MeshForward.X, MeshForward.Y, MeshForward.Z)};
		GEngine->AddOnScreenDebugMessage(2, 10.f, FColor::Blue, DebugMessage);
		DebugMessage = {FString::Printf(TEXT("Impulse:%f %f %f"), ImpulseVector.X, ImpulseVector.Y, ImpulseVector.Z)};
		GEngine->AddOnScreenDebugMessage(3, 10.f, FColor::Blue, DebugMessage);
	}*/
	
	float RandomRotation{-30.f}; //Use FMath::FRandRange to randomize
	ImpulseVector = ImpulseVector.RotateAngleAxis(RandomRotation, FVector{0.f, 0.f, 1.f});
	ImpulseVector *= 10000.f;

	if(GetRootComponent() == GetItemMesh())
	{
		GetItemMesh()->AddImpulse(ImpulseVector);
	}

	bFalling = true;
	GetWorldTimerManager().SetTimer(ThrowWeaponTimer, this, &AWeapon::StopFalling, ThrowWeaponTime);
}

void AWeapon::DecrementAmmo()
{
	if(Ammo - 1 <= 0)
	{
		Ammo = 0;
	}
	else
	{
		--Ammo;
	}
}

void AWeapon::ReloadAmmo(int32 Amount)
{
	checkf(Ammo + Amount <= MagazineCapacity, TEXT("Attempted to reload with more than magazine capacity"))
	Ammo += Amount;
}

void AWeapon::StopFalling()
{
	bFalling = false;
	SetItemState(EItemState::EIS_Pickup);
}
