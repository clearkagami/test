﻿using UnrealBuildTool;

public class NewAssetEditor : ModuleRules
{
	public NewAssetEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] {"Core"});
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject", "InputCore", "Engine", "Slate", "SlateCore", "UnrealEd", "NewAsset", "PropertyEditor"
			}
			);
	}
}