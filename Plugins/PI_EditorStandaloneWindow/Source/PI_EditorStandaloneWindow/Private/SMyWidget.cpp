﻿#include "SMyWidget.h"

#include "Widgets/Layout/SGridPanel.h"
#include "Widgets/Input/SSlider.h"

void SMyWidget::Construct(const FArguments& InArgs)
{
	int32 BoxID = 0;
	CheckBoxs.SetNum(2);
	SUserWidget::Construct(SUserWidget::FArguments().HAlign(HAlign_Fill).VAlign(VAlign_Fill)
		[
			SNew(SGridPanel)
			.FillColumn(0, 1).FillColumn(1, 3)
			.FillRow(0, 1).FillRow(1, 1).FillRow(2, 1).FillRow(3, 1)
			+ SGridPanel::Slot(0, 0).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(STextBlock).Text(FText::FromString(TEXT("百分比")))
			]
			+ SGridPanel::Slot(0, 1).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(STextBlock).Text(FText::FromString(TEXT("二选一")))
			]
			+ SGridPanel::Slot(0, 2).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(STextBlock).Text(FText::FromString(TEXT("下拉选项")))
			]
			+ SGridPanel::Slot(1, 0).HAlign(HAlign_Fill).VAlign(VAlign_Center)
			[
				SNew(SHorizontalBox)
				+SHorizontalBox::Slot().FillWidth(3).HAlign(HAlign_Fill).VAlign(VAlign_Center)
				[
					SAssignNew(Slider, SSlider)
				]
				+SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SNew(STextBlock).Text_Lambda(
						[this]()
						{
							return FText::FromString(FString::SanitizeFloat(Slider->GetValue()));
						}
						)
				]
			]
			+ SGridPanel::Slot(1, 1).HAlign(HAlign_Fill).VAlign(VAlign_Center)
			[
				SNew(SHorizontalBox)
				+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SAssignNew(CheckBoxs[0], SCheckBox)
					.IsChecked(true)
					.OnCheckStateChanged(
						FOnCheckStateChanged::CreateLambda(
							[this](ECheckBoxState NewState)
							{
								switch(NewState)
								{
									case ECheckBoxState::Unchecked:
										CheckBoxs[0]->SetIsChecked(ECheckBoxState::Checked);
										break;
									case ECheckBoxState::Checked:
										CheckBoxs[1]->SetIsChecked(ECheckBoxState::Unchecked);
									default:
										break;
								}
							}
							)
						)
						[
							SNew(STextBlock).Text(FText::FromString(TEXT("选项一")))
						]
				]
				+ SHorizontalBox::Slot().FillWidth(1).HAlign(HAlign_Center).VAlign(VAlign_Center)
				[
					SAssignNew(CheckBoxs[1], SCheckBox)
					.IsChecked(false)
					.OnCheckStateChanged(
						FOnCheckStateChanged::CreateLambda(
							[this](ECheckBoxState NewState)
							{
								switch(NewState)
								{
									case ECheckBoxState::Unchecked:
										CheckBoxs[1]->SetIsChecked(ECheckBoxState::Checked);
										break;
									case ECheckBoxState::Checked:
										CheckBoxs[0]->SetIsChecked(ECheckBoxState::Unchecked);
									default:
										break;
								}
							}
							)
						)
						[
							SNew(STextBlock).Text(FText::FromString(TEXT("选项二")))
						]
				]
			]
			+ SGridPanel::Slot(1, 2).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(SComboBox<TSharedPtr<int32>>)
				.OptionsSource(
					new TArray<TSharedPtr<int32>>{
						MakeShareable(new int32(0)),
						MakeShareable(new int32(1)),
						MakeShareable(new int32(2)),
						}
					)
				.OnGenerateWidget_Lambda(
					[this](TSharedPtr<int32> Int)
					{
						switch(*Int)
						{
							case 0:
								return SNew(STextBlock).Text(FText::FromString(TEXT("选项一")));
							case 1:
								return SNew(STextBlock).Text(FText::FromString(TEXT("选项二")));
							case 2:
								return SNew(STextBlock).Text(FText::FromString(TEXT("选项三")));
							default:
								return SNew(STextBlock).Text(FText::FromString(TEXT("空")));
						}
					}
					)
				.OnSelectionChanged_Lambda(
					[this](TSharedPtr<int32> Int, ESelectInfo::Type Type)
					{
						if(ComboText.IsValid())
						{
							switch(*Int)
							{
								case 0:
									ComboText->SetText(FText::FromString(TEXT("选项一")));
									break;
								case 1:
									ComboText->SetText(FText::FromString(TEXT("选项二")));
									break;
								case 2:
									ComboText->SetText(FText::FromString(TEXT("选项三")));
									break;
								default:
									ComboText->SetText(FText::FromString(TEXT("空")));
									break;
							}
						}
					}
					)
				[
					SAssignNew(ComboText, STextBlock)
				]
			]	
			+ SGridPanel::Slot(1, 3).HAlign(HAlign_Center).VAlign(VAlign_Center)
			[
				SNew(SButton)
				[
					SNew(STextBlock).Text(FText::FromString(TEXT("确定")))
				]
			]
		]
	);
}

TSharedRef<SMyWidget> SMyWidget::New()
{
	return MakeShareable(new SMyWidget());
}