// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NEWPLUGINMODULE_NewPluginClass_generated_h
#error "NewPluginClass.generated.h already included, missing '#pragma once' in NewPluginClass.h"
#endif
#define NEWPLUGINMODULE_NewPluginClass_generated_h

#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_SPARSE_DATA
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInvokeNewPluginClass);


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInvokeNewPluginClass);


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANewPluginClass(); \
	friend struct Z_Construct_UClass_ANewPluginClass_Statics; \
public: \
	DECLARE_CLASS(ANewPluginClass, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NewPluginModule"), NO_API) \
	DECLARE_SERIALIZER(ANewPluginClass)


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_INCLASS \
private: \
	static void StaticRegisterNativesANewPluginClass(); \
	friend struct Z_Construct_UClass_ANewPluginClass_Statics; \
public: \
	DECLARE_CLASS(ANewPluginClass, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NewPluginModule"), NO_API) \
	DECLARE_SERIALIZER(ANewPluginClass)


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANewPluginClass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANewPluginClass) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANewPluginClass); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANewPluginClass); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANewPluginClass(ANewPluginClass&&); \
	NO_API ANewPluginClass(const ANewPluginClass&); \
public:


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANewPluginClass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANewPluginClass(ANewPluginClass&&); \
	NO_API ANewPluginClass(const ANewPluginClass&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANewPluginClass); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANewPluginClass); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANewPluginClass)


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_6_PROLOG
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_SPARSE_DATA \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_INCLASS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_SPARSE_DATA \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NEWPLUGINMODULE_API UClass* StaticClass<class ANewPluginClass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_NewPluginModule_Public_NewPluginClass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
