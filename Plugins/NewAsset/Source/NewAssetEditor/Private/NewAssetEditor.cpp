﻿#pragma once
#include "NewAssetEditor.h"
#include "Modules/ModuleManager.h"


#include "AssetToolsModule.h"
#include "IAssetTools.h"
#include "NewAssetAction.h"
#include "NewAssetDetailCustomization.h"


#define LOCTEXT_NAMESPACE "FNewAssetEditorModule"

void FNewAssetEditorModule::StartupModule()
{
	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
	Action = MakeShareable(new FNewAssetAction());
	AssetTools.RegisterAssetTypeActions(Action.ToSharedRef());

	FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyEditorModule.RegisterCustomClassLayout(
		"NewAsset",
		FOnGetDetailCustomizationInstance::CreateLambda(
			[](){return MakeShareable(new FNewAssetDetailCustomization);}
			)
		);
	PropertyEditorModule.NotifyCustomizationModuleChanged();
}

void FNewAssetEditorModule::ShutdownModule()
{
	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
	if(Action.IsValid())
	{
		AssetTools.UnregisterAssetTypeActions(Action.ToSharedRef());
	}
}
	
#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FNewAssetEditorModule, NewAssetEditor);