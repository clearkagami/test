﻿using UnrealBuildTool;

public class NewPluginModule : ModuleRules
{
	public NewPluginModule(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] {"Core", "CoreUObject", "InputCore", "Engine"});
		PrivateDependencyModuleNames.AddRange(new string[] {"PI_ThirdPartyLibrary"});
	}
}