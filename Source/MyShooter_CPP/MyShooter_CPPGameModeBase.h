// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyShooter_CPPGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYSHOOTER_CPP_API AMyShooter_CPPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
};
