// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_EditorToolbarButton.h"
#include "PI_EditorToolbarButtonStyle.h"
#include "PI_EditorToolbarButtonCommands.h"
#include "Misc/MessageDialog.h"
#include "ToolMenus.h"
#include "IAnimationBlueprintEditorModule.h"
#include "BlueprintEditorModule.h"


static const FName PI_EditorToolbarButtonTabName("PI_EditorToolbarButton");

#define LOCTEXT_NAMESPACE "FPI_EditorToolbarButtonModule"

void FPI_EditorToolbarButtonModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FPI_EditorToolbarButtonStyle::Initialize();
	FPI_EditorToolbarButtonStyle::ReloadTextures();

	FPI_EditorToolbarButtonCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FPI_EditorToolbarButtonCommands::Get().PluginAction,
		FExecuteAction::CreateRaw(this, &FPI_EditorToolbarButtonModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FPI_EditorToolbarButtonModule::RegisterMenus));
}

void FPI_EditorToolbarButtonModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FPI_EditorToolbarButtonStyle::Shutdown();

	FPI_EditorToolbarButtonCommands::Unregister();
}

void FPI_EditorToolbarButtonModule::PluginButtonClicked()
{
	// Put your "OnButtonClicked" stuff here
	FText DialogText = FText::Format(
							LOCTEXT("PluginButtonDialogText", "Add code to {0} in {1} to override this button's actions"),
							FText::FromString(TEXT("FPI_EditorToolbarButtonModule::PluginButtonClicked()")),
							FText::FromString(TEXT("PI_EditorToolbarButton.cpp"))
					   );
	FMessageDialog::Open(EAppMsgType::Ok, DialogText);
}

void FPI_EditorToolbarButtonModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	//获取到section直接进行扩展
	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FPI_EditorToolbarButtonCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FPI_EditorToolbarButtonCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
				Entry.InsertPosition.Position = EToolMenuInsertType::First;
			}
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Modes");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FPI_EditorToolbarButtonCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
				Entry.InsertPosition.Position = EToolMenuInsertType::First;
			}
		}
	}

	{
		UToolMenu* MyMenu = UToolMenus::Get()->RegisterMenu("LevelEditor.MainMenu.MyNewMenu");
		{
			FToolMenuSection& Section = MyMenu->FindOrAddSection("NewSection");
			{
				FToolMenuEntry& Entry = Section.AddMenuEntryWithCommandList(
					FPI_EditorToolbarButtonCommands::Get().PluginAction,
					PluginCommands
					);
				
				UToolMenu* MainMenuBar = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu");
				MainMenuBar->AddSubMenu("MainMenu", "MySection", "MyNewMenu", LOCTEXT("Menu", "MyNewMenu"));
				
			}
		}
	}
	
	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("AssetEditor.BlueprintEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FPI_EditorToolbarButtonCommands::Get().PluginAction, PluginCommands);
		}
	}
		
	//使用extender进行扩展
	IAnimationBlueprintEditorModule& AnimationBlueprintEditorModule
		= FModuleManager::LoadModuleChecked<IAnimationBlueprintEditorModule>("AnimationBlueprintEditor");
	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuExtension(
			"HelpApplication",
			EExtensionHook::After,
			PluginCommands,
			FMenuExtensionDelegate::CreateRaw(this, &FPI_EditorToolbarButtonModule::AddMenuExtension)
			);
		AnimationBlueprintEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}

	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuBarExtension(
			"Help",
			EExtensionHook::After,
			PluginCommands,
			FMenuBarExtensionDelegate::CreateRaw(this, &FPI_EditorToolbarButtonModule::AddMenuBarExtension)
			);
		AnimationBlueprintEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}

	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddToolBarExtension(
			"Settings",
			EExtensionHook::After,
			PluginCommands,
			FToolBarExtensionDelegate::CreateRaw(this, &FPI_EditorToolbarButtonModule::AddToolBarExtension)
			);
		AnimationBlueprintEditorModule.GetToolBarExtensibilityManager()->AddExtender(MenuExtender);
	}

	FBlueprintEditorModule& BlueprintEditorModule = FModuleManager::LoadModuleChecked<FBlueprintEditorModule>(TEXT("Kismet"));
	{
		TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuExtension(
			"HelpApplication",
			EExtensionHook::After,
			PluginCommands,
			FMenuExtensionDelegate::CreateRaw(this, &FPI_EditorToolbarButtonModule::AddMenuExtension)
			);
		BlueprintEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);
	}

	{
		BlueprintEditorModule.OnRegisterTabsForEditor().AddRaw(
			this,
			&FPI_EditorToolbarButtonModule::FuncOnBlueprintToolBarRegister
			);
	}
}

void FPI_EditorToolbarButtonModule::AddMenuExtension(class FMenuBuilder& Builder)
{
	Builder.BeginSection(TEXT("MyNewSection"));
	Builder.AddMenuEntry(FPI_EditorToolbarButtonCommands::Get().PluginAction);
	Builder.EndSection();
}

void FPI_EditorToolbarButtonModule::AddMenuBarExtension(FMenuBarBuilder& Builder)
{
	Builder.AddMenuEntry(FPI_EditorToolbarButtonCommands::Get().PluginAction);
}

void FPI_EditorToolbarButtonModule::AddToolBarExtension(FToolBarBuilder& Builder)
{
	Builder.BeginSection(TEXT("MyNewSection"));
	Builder.AddToolBarButton(FPI_EditorToolbarButtonCommands::Get().PluginAction);
	Builder.EndSection();
}

void FPI_EditorToolbarButtonModule::FuncOnBlueprintToolBarRegister(FWorkflowAllowedTabSet& WorkflowAllowedTabSet,
	FName Name, TSharedPtr<FBlueprintEditor> BlueprintEditor)
{
	TSharedPtr<FExtender> MenuExtender = MakeShareable(new FExtender);
	MenuExtender->AddToolBarExtension(
		"Settings",
		EExtensionHook::After,
		PluginCommands,
		FToolBarExtensionDelegate::CreateRaw(this, &FPI_EditorToolbarButtonModule::AddToolBarExtension)
		);
	BlueprintEditor->AddToolbarExtender(MenuExtender);
}


#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPI_EditorToolbarButtonModule, PI_EditorToolbarButton)