// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PI_ThirdPartyLibrary/Public/ThirdLibInvoker.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeThirdLibInvoker() {}
// Cross Module References
	PI_THIRDPARTYLIBRARY_API UClass* Z_Construct_UClass_UThirdLibInvoker_NoRegister();
	PI_THIRDPARTYLIBRARY_API UClass* Z_Construct_UClass_UThirdLibInvoker();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_PI_ThirdPartyLibrary();
// End Cross Module References
	void UThirdLibInvoker::StaticRegisterNativesUThirdLibInvoker()
	{
	}
	UClass* Z_Construct_UClass_UThirdLibInvoker_NoRegister()
	{
		return UThirdLibInvoker::StaticClass();
	}
	struct Z_Construct_UClass_UThirdLibInvoker_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UThirdLibInvoker_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PI_ThirdPartyLibrary,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UThirdLibInvoker_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ThirdLibInvoker.h" },
		{ "ModuleRelativePath", "Public/ThirdLibInvoker.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UThirdLibInvoker_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UThirdLibInvoker>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UThirdLibInvoker_Statics::ClassParams = {
		&UThirdLibInvoker::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UThirdLibInvoker_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UThirdLibInvoker_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UThirdLibInvoker()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UThirdLibInvoker_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UThirdLibInvoker, 2115462691);
	template<> PI_THIRDPARTYLIBRARY_API UClass* StaticClass<UThirdLibInvoker>()
	{
		return UThirdLibInvoker::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UThirdLibInvoker(Z_Construct_UClass_UThirdLibInvoker, &UThirdLibInvoker::StaticClass, TEXT("/Script/PI_ThirdPartyLibrary"), TEXT("UThirdLibInvoker"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UThirdLibInvoker);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
