// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NewPluginModule/Public/NewPluginClass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNewPluginClass() {}
// Cross Module References
	NEWPLUGINMODULE_API UClass* Z_Construct_UClass_ANewPluginClass_NoRegister();
	NEWPLUGINMODULE_API UClass* Z_Construct_UClass_ANewPluginClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_NewPluginModule();
// End Cross Module References
	DEFINE_FUNCTION(ANewPluginClass::execInvokeNewPluginClass)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InvokeNewPluginClass();
		P_NATIVE_END;
	}
	void ANewPluginClass::StaticRegisterNativesANewPluginClass()
	{
		UClass* Class = ANewPluginClass::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "InvokeNewPluginClass", &ANewPluginClass::execInvokeNewPluginClass },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NewPluginClass.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANewPluginClass, nullptr, "InvokeNewPluginClass", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANewPluginClass_NoRegister()
	{
		return ANewPluginClass::StaticClass();
	}
	struct Z_Construct_UClass_ANewPluginClass_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANewPluginClass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NewPluginModule,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANewPluginClass_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANewPluginClass_InvokeNewPluginClass, "InvokeNewPluginClass" }, // 1361572384
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANewPluginClass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "NewPluginClass.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NewPluginClass.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANewPluginClass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANewPluginClass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANewPluginClass_Statics::ClassParams = {
		&ANewPluginClass::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANewPluginClass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANewPluginClass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANewPluginClass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANewPluginClass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANewPluginClass, 405345144);
	template<> NEWPLUGINMODULE_API UClass* StaticClass<ANewPluginClass>()
	{
		return ANewPluginClass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANewPluginClass(Z_Construct_UClass_ANewPluginClass, &ANewPluginClass::StaticClass, TEXT("/Script/NewPluginModule"), TEXT("ANewPluginClass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANewPluginClass);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
