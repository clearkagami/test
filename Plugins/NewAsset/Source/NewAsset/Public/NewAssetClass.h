﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "NewAssetClass.generated.h"

UCLASS(BlueprintType, Blueprintable)
class NEWASSET_API UNewAsset : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NewAsset)
	int32 IntValue;

	UFUNCTION(BlueprintCallable)
	void Inc()
	{
		++IntValue;
	}

};