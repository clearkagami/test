// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NewAssetEditor/Public/NewAssetFactoryFile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNewAssetFactoryFile() {}
// Cross Module References
	NEWASSETEDITOR_API UClass* Z_Construct_UClass_UNewAssetFactoryFile_NoRegister();
	NEWASSETEDITOR_API UClass* Z_Construct_UClass_UNewAssetFactoryFile();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_NewAssetEditor();
// End Cross Module References
	void UNewAssetFactoryFile::StaticRegisterNativesUNewAssetFactoryFile()
	{
	}
	UClass* Z_Construct_UClass_UNewAssetFactoryFile_NoRegister()
	{
		return UNewAssetFactoryFile::StaticClass();
	}
	struct Z_Construct_UClass_UNewAssetFactoryFile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNewAssetFactoryFile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NewAssetEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewAssetFactoryFile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NewAssetFactoryFile.h" },
		{ "ModuleRelativePath", "Public/NewAssetFactoryFile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNewAssetFactoryFile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNewAssetFactoryFile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNewAssetFactoryFile_Statics::ClassParams = {
		&UNewAssetFactoryFile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNewAssetFactoryFile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNewAssetFactoryFile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNewAssetFactoryFile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNewAssetFactoryFile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNewAssetFactoryFile, 2559804910);
	template<> NEWASSETEDITOR_API UClass* StaticClass<UNewAssetFactoryFile>()
	{
		return UNewAssetFactoryFile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNewAssetFactoryFile(Z_Construct_UClass_UNewAssetFactoryFile, &UNewAssetFactoryFile::StaticClass, TEXT("/Script/NewAssetEditor"), TEXT("UNewAssetFactoryFile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNewAssetFactoryFile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
