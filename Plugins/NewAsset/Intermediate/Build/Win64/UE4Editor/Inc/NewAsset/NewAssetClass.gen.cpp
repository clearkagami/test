// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NewAsset/Public/NewAssetClass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNewAssetClass() {}
// Cross Module References
	NEWASSET_API UClass* Z_Construct_UClass_UNewAsset_NoRegister();
	NEWASSET_API UClass* Z_Construct_UClass_UNewAsset();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NewAsset();
// End Cross Module References
	DEFINE_FUNCTION(UNewAsset::execInc)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Inc();
		P_NATIVE_END;
	}
	void UNewAsset::StaticRegisterNativesUNewAsset()
	{
		UClass* Class = UNewAsset::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Inc", &UNewAsset::execInc },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNewAsset_Inc_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNewAsset_Inc_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NewAssetClass.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNewAsset_Inc_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNewAsset, nullptr, "Inc", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNewAsset_Inc_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNewAsset_Inc_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNewAsset_Inc()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNewAsset_Inc_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNewAsset_NoRegister()
	{
		return UNewAsset::StaticClass();
	}
	struct Z_Construct_UClass_UNewAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNewAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NewAsset,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNewAsset_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNewAsset_Inc, "Inc" }, // 3624029741
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewAsset_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "NewAssetClass.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NewAssetClass.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewAsset_Statics::NewProp_IntValue_MetaData[] = {
		{ "Category", "NewAsset" },
		{ "ModuleRelativePath", "Public/NewAssetClass.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNewAsset_Statics::NewProp_IntValue = { "IntValue", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNewAsset, IntValue), METADATA_PARAMS(Z_Construct_UClass_UNewAsset_Statics::NewProp_IntValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNewAsset_Statics::NewProp_IntValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNewAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNewAsset_Statics::NewProp_IntValue,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNewAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNewAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNewAsset_Statics::ClassParams = {
		&UNewAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UNewAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UNewAsset_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNewAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNewAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNewAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNewAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNewAsset, 3086161004);
	template<> NEWASSET_API UClass* StaticClass<UNewAsset>()
	{
		return UNewAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNewAsset(Z_Construct_UClass_UNewAsset, &UNewAsset::StaticClass, TEXT("/Script/NewAsset"), TEXT("UNewAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNewAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
