// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAnimInstance.h"
#include "ShooterCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"


UShooterAnimInstance::UShooterAnimInstance():
	Speed(0.f),
	bIsInAir(false),
	bIsAccelerating(false),
	DeltaYawMovementFromAim(0.f),
	LastDeltaYawMovementFromAim(0.f),
	bAiming(false),
	TIPCharacterYaw(0.f),
	TIPCharacterYawLastFrame(0.f),
	RootYawOffset(0.f),
	Pitch(0.f),
	bReloading(false),
	CharacterYaw(0.f),
	CharacterYawLastFrame(0.f),
	YawDeltaRate(0.f)
{
	
}

void UShooterAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	ShooterCharacter = Cast<AShooterCharacter>(TryGetPawnOwner());

	UE_LOG(LogTemp, Warning, TEXT("NativeInitializeAnimation"))
}

void UShooterAnimInstance::UpdateAnimProperties(float DeltaTime)
{
	if(ShooterCharacter)
	{
		bReloading = ShooterCharacter->GetCombatState() == ECombatState::ECS_Reloading;

		/*Update Speed from velocity of the character*/
		FVector Velocity{ShooterCharacter->GetVelocity()};
		Velocity.Z = 0;
		Speed = Velocity.Size();

		/*Update bIsInAir*/
		bIsInAir = ShooterCharacter->GetCharacterMovement()->IsFalling();

		/*Update bIsAccelerating*/
		if(ShooterCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size()>0.f)
		{
			bIsAccelerating = true;
		}
		else
		{
			bIsAccelerating = false;
		}

		FRotator AimRotation{ShooterCharacter->GetBaseAimRotation()};
		

		
		FRotator MovementRotation{FRotationMatrix::MakeFromX(ShooterCharacter->GetVelocity()).Rotator()};
		//Also can use UKismetMathLibrary::MakeRotFromX
		
		DeltaYawMovementFromAim =  FRotator{MovementRotation - AimRotation}.GetNormalized().Yaw;
		//Also can use UKismetMathLibrary::NormalizedDeltaRotator

		if(bIsAccelerating)
		{
			LastDeltaYawMovementFromAim = DeltaYawMovementFromAim;
		}

		bAiming = ShooterCharacter->GetAiming();

	}

	TurnInPlace();
	Lean(DeltaTime);
}

void UShooterAnimInstance::TurnInPlace()
{
	if(ShooterCharacter == nullptr) return;

	Pitch = ShooterCharacter->GetBaseAimRotation().Pitch;
	
	if(Speed > 0 || bIsInAir)
	{
		/*Don't turn in place*/
		RootYawOffset = 0.f;

		/*Reset values*/
		TIPCharacterYaw = ShooterCharacter->GetActorRotation().Yaw;
		RotationCurve = 0.f;
		
	}
	else
	{
		TIPCharacterYawLastFrame = TIPCharacterYaw;
		TIPCharacterYaw = ShooterCharacter->GetActorRotation().Yaw;
		const float TIPYawDelta{TIPCharacterYaw - TIPCharacterYawLastFrame};

        /*Root Yaw Offset, updated and clamped to [-180, 180]*/
		RootYawOffset = UKismetMathLibrary::NormalizeAxis(RootYawOffset - TIPYawDelta);

		const float Turning{GetCurveValue(TEXT("Turning"))};
		if(Turning) 
		{
			RotationCurveLastFrame = RotationCurve;
			RotationCurve = GetCurveValue(TEXT("Rotation"));
			const float DeltaRotation{RotationCurve - RotationCurveLastFrame};
					
			if(RootYawOffset > 0) /*Turning left*/
			{
				RootYawOffset -= DeltaRotation;
			}
			else /*Turning right*/
			{
				RootYawOffset += DeltaRotation;
			}

			/*This Clamp has two effects.
			 *When first frame in RotationCurve,
			 *DeltaRotation would be -90 because our curves set from -90 to 0.
			 *And RotationCurveLastFrame  would be 0, RotationCurve would be -90.
			 *So RootYawOffset would be increased or decreased a useless 90.
			 *This Clamp could solve this problem at first.*/
			RootYawOffset = FMath::Clamp(RootYawOffset, -90.f, 90.f);
	
		if(GEngine) GEngine->AddOnScreenDebugMessage(
			2,
			-1,
			FColor::Yellow,
			FString::Printf(TEXT("Turn In Place")));	
		}
		if(GEngine) GEngine->AddOnScreenDebugMessage(
			1,
			-1,
			FColor::Blue,
			FString::Printf(TEXT("RootYawOffset: %f"), RootYawOffset));
		
	}
}

void UShooterAnimInstance::Lean(float DeltaTime)
{
	if(ShooterCharacter == nullptr) return;

	CharacterYawLastFrame = CharacterYaw;
	CharacterYaw = ShooterCharacter->GetActorRotation().Yaw;
	const float LeanYawDelta = UKismetMathLibrary::NormalizeAxis(CharacterYaw - CharacterYawLastFrame);
	
	const float Target{LeanYawDelta / DeltaTime};
	const float Interp{FMath::FInterpTo(YawDeltaRate, Target, DeltaTime, 5.f)};

	YawDeltaRate = FMath::Clamp(Interp, -90.f, 90.f);

	if(GEngine) GEngine->AddOnScreenDebugMessage(
		3,
		-1,
		FColor::Green,
		FString::Printf(TEXT("YawDeltaRate: %f"), YawDeltaRate));	
}
