﻿#pragma once
#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "NicknamedActor.generated.h"

UCLASS(Blueprintable)
class FOOBAR_API ANicknamedActor : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Nickname;

	UFUNCTION(BlueprintCallable)
	void InvokeNickname();
};