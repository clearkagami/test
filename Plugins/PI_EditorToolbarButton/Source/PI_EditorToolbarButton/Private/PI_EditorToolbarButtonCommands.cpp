// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_EditorToolbarButtonCommands.h"

#define LOCTEXT_NAMESPACE "FPI_EditorToolbarButtonModule"

void FPI_EditorToolbarButtonCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "PI_EditorToolbarButton", "Execute PI_EditorToolbarButton action", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
