// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_EditorStandaloneWindowCommands.h"

#define LOCTEXT_NAMESPACE "FPI_EditorStandaloneWindowModule"

void FPI_EditorStandaloneWindowCommands::RegisterCommands()
{
	UI_COMMAND(OpenPluginWindow, "PI_EditorStandaloneWindow", "Bring up PI_EditorStandaloneWindow window", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
