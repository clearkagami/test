// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PI_THIRDPARTYLIBRARY_ThirdLibInvoker_generated_h
#error "ThirdLibInvoker.generated.h already included, missing '#pragma once' in ThirdLibInvoker.h"
#endif
#define PI_THIRDPARTYLIBRARY_ThirdLibInvoker_generated_h

#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_SPARSE_DATA
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_RPC_WRAPPERS
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUThirdLibInvoker(); \
	friend struct Z_Construct_UClass_UThirdLibInvoker_Statics; \
public: \
	DECLARE_CLASS(UThirdLibInvoker, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PI_ThirdPartyLibrary"), NO_API) \
	DECLARE_SERIALIZER(UThirdLibInvoker)


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUThirdLibInvoker(); \
	friend struct Z_Construct_UClass_UThirdLibInvoker_Statics; \
public: \
	DECLARE_CLASS(UThirdLibInvoker, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PI_ThirdPartyLibrary"), NO_API) \
	DECLARE_SERIALIZER(UThirdLibInvoker)


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UThirdLibInvoker(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UThirdLibInvoker) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UThirdLibInvoker); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UThirdLibInvoker); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UThirdLibInvoker(UThirdLibInvoker&&); \
	NO_API UThirdLibInvoker(const UThirdLibInvoker&); \
public:


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UThirdLibInvoker(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UThirdLibInvoker(UThirdLibInvoker&&); \
	NO_API UThirdLibInvoker(const UThirdLibInvoker&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UThirdLibInvoker); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UThirdLibInvoker); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UThirdLibInvoker)


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_8_PROLOG
#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_SPARSE_DATA \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_INCLASS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_SPARSE_DATA \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PI_THIRDPARTYLIBRARY_API UClass* StaticClass<class UThirdLibInvoker>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_PI_ThirdPartyLibrary_Source_PI_ThirdPartyLibrary_Public_ThirdLibInvoker_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
