// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_EditorModeEdMode.h"
#include "PI_EditorModeEdModeToolkit.h"
#include "Toolkits/ToolkitManager.h"
#include "EditorModeManager.h"

const FEditorModeID FPI_EditorModeEdMode::EM_PI_EditorModeEdModeId = TEXT("EM_PI_EditorModeEdMode");

FPI_EditorModeEdMode::FPI_EditorModeEdMode()
{

}

FPI_EditorModeEdMode::~FPI_EditorModeEdMode()
{

}

void FPI_EditorModeEdMode::Enter()
{
	FEdMode::Enter();

	if (!Toolkit.IsValid() && UsesToolkits())
	{
		Toolkit = MakeShareable(new FPI_EditorModeEdModeToolkit);
		Toolkit->Init(Owner->GetToolkitHost());
	}
}

void FPI_EditorModeEdMode::Exit()
{
	if (Toolkit.IsValid())
	{
		FToolkitManager::Get().CloseToolkit(Toolkit.ToSharedRef());
		Toolkit.Reset();
	}

	// Call base Exit method to ensure proper cleanup
	FEdMode::Exit();
}

bool FPI_EditorModeEdMode::UsesToolkits() const
{
	return true;
}




