// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "PI_EditorStandaloneWindowStyle.h"

class FPI_EditorStandaloneWindowCommands : public TCommands<FPI_EditorStandaloneWindowCommands>
{
public:

	FPI_EditorStandaloneWindowCommands()
		: TCommands<FPI_EditorStandaloneWindowCommands>(TEXT("PI_EditorStandaloneWindow"), NSLOCTEXT("Contexts", "PI_EditorStandaloneWindow", "PI_EditorStandaloneWindow Plugin"), NAME_None, FPI_EditorStandaloneWindowStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > OpenPluginWindow;
};