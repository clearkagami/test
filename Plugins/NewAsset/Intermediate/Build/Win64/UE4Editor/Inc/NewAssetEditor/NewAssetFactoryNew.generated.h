// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NEWASSETEDITOR_NewAssetFactoryNew_generated_h
#error "NewAssetFactoryNew.generated.h already included, missing '#pragma once' in NewAssetFactoryNew.h"
#endif
#define NEWASSETEDITOR_NewAssetFactoryNew_generated_h

#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_SPARSE_DATA
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_RPC_WRAPPERS
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNewAssetFactoryNew(); \
	friend struct Z_Construct_UClass_UNewAssetFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNewAssetFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NewAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UNewAssetFactoryNew)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUNewAssetFactoryNew(); \
	friend struct Z_Construct_UClass_UNewAssetFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UNewAssetFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NewAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UNewAssetFactoryNew)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNewAssetFactoryNew(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNewAssetFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAssetFactoryNew); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAssetFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAssetFactoryNew(UNewAssetFactoryNew&&); \
	NO_API UNewAssetFactoryNew(const UNewAssetFactoryNew&); \
public:


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNewAssetFactoryNew(UNewAssetFactoryNew&&); \
	NO_API UNewAssetFactoryNew(const UNewAssetFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNewAssetFactoryNew); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNewAssetFactoryNew); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNewAssetFactoryNew)


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_7_PROLOG
#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_INCLASS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_SPARSE_DATA \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NEWASSETEDITOR_API UClass* StaticClass<class UNewAssetFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_NewAsset_Source_NewAssetEditor_Public_NewAssetFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
