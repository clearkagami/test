// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PI_BLUEPRINTLIBRARY_PI_BlueprintLibraryBPLibrary_generated_h
#error "PI_BlueprintLibraryBPLibrary.generated.h already included, missing '#pragma once' in PI_BlueprintLibraryBPLibrary.h"
#endif
#define PI_BLUEPRINTLIBRARY_PI_BlueprintLibraryBPLibrary_generated_h

#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_SPARSE_DATA
#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPI_BlueprintLibrarySampleFunction);


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPI_BlueprintLibrarySampleFunction);


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPI_BlueprintLibraryBPLibrary(); \
	friend struct Z_Construct_UClass_UPI_BlueprintLibraryBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UPI_BlueprintLibraryBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PI_BlueprintLibrary"), NO_API) \
	DECLARE_SERIALIZER(UPI_BlueprintLibraryBPLibrary)


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUPI_BlueprintLibraryBPLibrary(); \
	friend struct Z_Construct_UClass_UPI_BlueprintLibraryBPLibrary_Statics; \
public: \
	DECLARE_CLASS(UPI_BlueprintLibraryBPLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PI_BlueprintLibrary"), NO_API) \
	DECLARE_SERIALIZER(UPI_BlueprintLibraryBPLibrary)


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPI_BlueprintLibraryBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPI_BlueprintLibraryBPLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPI_BlueprintLibraryBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPI_BlueprintLibraryBPLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPI_BlueprintLibraryBPLibrary(UPI_BlueprintLibraryBPLibrary&&); \
	NO_API UPI_BlueprintLibraryBPLibrary(const UPI_BlueprintLibraryBPLibrary&); \
public:


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPI_BlueprintLibraryBPLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPI_BlueprintLibraryBPLibrary(UPI_BlueprintLibraryBPLibrary&&); \
	NO_API UPI_BlueprintLibraryBPLibrary(const UPI_BlueprintLibraryBPLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPI_BlueprintLibraryBPLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPI_BlueprintLibraryBPLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPI_BlueprintLibraryBPLibrary)


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_PRIVATE_PROPERTY_OFFSET
#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_25_PROLOG
#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_SPARSE_DATA \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_RPC_WRAPPERS \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_INCLASS \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_PRIVATE_PROPERTY_OFFSET \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_SPARSE_DATA \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_INCLASS_NO_PURE_DECLS \
	MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h_28_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PI_BlueprintLibraryBPLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PI_BLUEPRINTLIBRARY_API UClass* StaticClass<class UPI_BlueprintLibraryBPLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyShooter_CPP_Plugins_PI_BlueprintLibrary_Source_PI_BlueprintLibrary_Public_PI_BlueprintLibraryBPLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
