using UnrealBuildTool;

public class FooBar : ModuleRules
{
	public FooBar(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] {"CoreUObject", "InputCore", "Engine"});
		PrivateDependencyModuleNames.AddRange(new string[] {"Core", "NewPluginModule"});
	}
}