﻿#pragma once
#include "Toolkits/AssetEditorToolkit.h"

class FNewAssetToolkit : public FAssetEditorToolkit
{
public:
	virtual void RegisterTabSpawners(const TSharedRef<FTabManager>& TabManager) override;
	virtual void UnregisterTabSpawners(const TSharedRef<FTabManager>& TabManager) override;
	void Initialize(class UNewAsset* InNewAsset, const EToolkitMode::Type InMode, TSharedPtr<IToolkitHost> EditWithinLevelEditor);
	virtual FText GetBaseToolkitName() const override;
	virtual FName GetToolkitFName() const override;
	virtual FLinearColor GetWorldCentricTabColorScale() const override;
	virtual FString GetWorldCentricTabPrefix() const override;

	
private:
	class UNewAsset* NewAsset = nullptr;
	TSharedPtr<STextBlock> TextBlock;

	TSharedRef<SDockTab> OnSpawnNewAssetToolkit(const FSpawnTabArgs& SpawnTabArgs);

	TSharedPtr<class IDetailsView> DetailsView;
};

