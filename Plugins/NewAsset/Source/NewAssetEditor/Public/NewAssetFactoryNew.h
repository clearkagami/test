﻿#pragma once

#include "CoreMinimal.h"
#include "Factories/Factory.h"
#include "NewAssetFactoryNew.generated.h"

UCLASS()
class UNewAssetFactoryNew : public UFactory
{
	GENERATED_BODY()

public:
	UNewAssetFactoryNew();
	
	virtual  UObject* FactoryCreateNew(
		UClass* InClass,
		UObject* InParent,
		FName InName,
		EObjectFlags Flags,
		UObject* Context,
		FFeedbackContext* Warn
		) override;
	virtual  bool ShouldShowInNewMenu() const override;
};