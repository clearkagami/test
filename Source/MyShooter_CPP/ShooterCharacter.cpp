// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"

#include <filesystem>

#include "DrawDebugHelpers.h"
#include "Item.h"
#include "Components/WidgetComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Weapon.h"


// Sets default values
AShooterCharacter::AShooterCharacter():
	/*Base rates for turning/looking up*/
	BaseTurnRate(60.f),
	BaseLookUpRate(60.f),
	/*Camera field of view values*/
	CameraDefaultFOV(0.f), //set in BeginPlay
	CameraZoomedFOV(50.f),
	CameraCurrentFOV(0.f),
	ZoomInterpSpeed(20.f),
	/*Turn rates for aiming/not aiming*/
	HipTurnRate(60.f),
	HipLookUpRate(60.f),
	AimingTurnRate(20.f),
	AimingLookUpRate(20.f),
	/*Aiming*/
	bAiming(false),
	/*Cross Hair Spread Factors*/
	CrossHairSpreadMultiplier(0.f),
	CrossHairVelocityFactor(0.f),
	CrossHairInAirFactor(0.f),
	CrossHairAimFactor(0.f),
	CrossHairShootingFactor(0.f),
	/*Timer of Cross Hair Spread when firing*/
	ShootTimeDuration(0.05f),
	bFiringBullet(false),
	/*Automatic gun fire variables*/
	AutomaticFireRate(0.1f),
	bShouldFire(true),
	bFireButtonPressed(false),
	/*Item trace variables*/
	bShouldTraceForItems(false),
	ItemTraceLength(200.f),
	/*Camera interp location variables*/
	CameraInterpDistance(250.f),
	CameraInterpElevation(65.f),
	/*Starting ammo amounts*/
	Starting9mmAmmo(85),
	StartingARAmmo(120),
	/*CombatState variables*/
	CombatState(ECombatState::ECS_Unoccupied)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*Create a CameraBoom (pulls in camera towards the character if there is a collision)*/
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 160.f;
	CameraBoom->bUsePawnControlRotation = true; /*Set boom rotation follow the control rotation*/
	CameraBoom->SocketOffset = FVector{0.f, 50.f, 80.f};
	
	/*Create a FollowCamera*/
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); //Attach camera to end of boom
	FollowCamera->bUsePawnControlRotation = false; //Camera does not rotate relatively to arm

	/*Initialize whether or not the character rotation follow the control rotation*/
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = true;

	/*Configure character movement component*/
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator{0.f, 540.f, 0.f};
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	/*Create Hand Scene Component*/
	HandSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HandSceneComp"));
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	/*float mytestfloat{};
	UE_LOG(LogTemp, Warning, TEXT(": %d"), mytestfloat);*/

	JumpMaxHoldTime = 0.5f;

	if(FollowCamera)
	{
		CameraDefaultFOV = GetFollowCamera()->FieldOfView;
		CameraCurrentFOV = CameraDefaultFOV; 
	}

	/*Spawn the default weapon and equip it*/
	EquipWeapon(SpawnDefaultWeapon());

	InitializeAmmoMap();
}

void AShooterCharacter::MoveForward(float Value)
{
	if(Controller != nullptr)
	{
		if(Value != 0.0f)
		{
			/*Find out which way is forward*/
			const FRotator ControlRotation{Controller->GetControlRotation()};
			const FRotator ControlRotation_YawRemain{0, ControlRotation.Yaw, 0};
			const FVector ForwardDirection{FRotationMatrix{ControlRotation_YawRemain}.GetUnitAxis(EAxis::X)};

			AddMovementInput(ForwardDirection, Value);
		}
	}
}

void AShooterCharacter::MoveRight(float Value)
{
	if(Controller != nullptr)
	{
		if(Value != 0.0f)
		{
			/*Find out which way is Right*/
			const FRotator ControlRotation{Controller->GetControlRotation()};
			const FRotator ControlRotation_YawRemain{0, ControlRotation.Yaw, 0};
			const FVector RightDirection{FRotationMatrix{ControlRotation_YawRemain}.GetUnitAxis(EAxis::Y)};

			AddMovementInput(RightDirection, Value);
		}
	}
}

void AShooterCharacter::Turn(float Value)
{
	/*if(GEngine)
	{
		FString DebugMessage{FString::Printf(TEXT("%f"), BaseTurnRate)};
		GEngine->AddOnScreenDebugMessage(1, 0.f, FColor::White, DebugMessage);
	}*/
	AddControllerYawInput(BaseTurnRate * GetWorld()->GetDeltaSeconds() * Value);	/*degrees/s * seconds/f = degrees/f*/
}

void AShooterCharacter::LookUp(float Value)
{
	AddControllerPitchInput(BaseLookUpRate * GetWorld()->GetDeltaSeconds() * Value);
}

void AShooterCharacter::Fire()
{
	if(CombatState != ECombatState::ECS_Unoccupied) return;
	if(EquippedWeaponHasAmmo() == false) return;
		
	PlayFireSound();

	SendBullet();

	PlayGunFireMontage();
	
	/*Cross Hair Spread when Firing*/
	StartCrossHairBulletFire();

	/*Firing consumes Ammo of EquippedWeapon*/
	if(EquippedWeapon)
	{
		EquippedWeapon->DecrementAmmo();
	}

	StartFireTimer();
}

bool AShooterCharacter::DetermineFireHitLocation(const FVector& SocketLocation, FVector& OutHitLocation)
{
	/*First line trace from cross hair*/
	FHitResult CrossHairHitResult;
	bool bCrossHairHit = TraceUnderCrossHairs(CrossHairHitResult, 50000.f);
	
	OutHitLocation = CrossHairHitResult.TraceEnd;

	if(bCrossHairHit)
	{
		OutHitLocation = CrossHairHitResult.Location;
	}

	/*Second line trace from weapon barrel*/
	FHitResult WeaponBarrelTraceHit;
	const FVector WeaponBarrelTraceStart{SocketLocation};
	const FVector WeaponBarrelTraceEnd{OutHitLocation};
	GetWorld()->LineTraceSingleByChannel(
		WeaponBarrelTraceHit,
		WeaponBarrelTraceStart,
		WeaponBarrelTraceEnd,
		ECollisionChannel::ECC_Visibility);

	/*Final result of the HitLocation*/
	if(WeaponBarrelTraceHit.bBlockingHit)
	{
		OutHitLocation = WeaponBarrelTraceHit.Location;
	}

	return bCrossHairHit || WeaponBarrelTraceHit.bBlockingHit;
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*Interp FOV*/
	CameraInterpZoom(DeltaTime);
	/*Change look sensitivity based on aiming*/
	SetLookRates();
	/*Calculate cross hair spread multiplier*/
	CalculateCrossHairSpread(DeltaTime);
	/*Check OverlappedItemCount, then trace for items*/
	TraceForItems();
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) 
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	/*Bind functions*/
	PlayerInputComponent->BindAxis("MoveForward", this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &AShooterCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &AShooterCharacter::LookUp);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterCharacter::FireButtonReleased);

	PlayerInputComponent->BindAction("Aiming", IE_Pressed, this, &AShooterCharacter::AimingButtonPressed);
	PlayerInputComponent->BindAction(
		"Aiming",
		IE_Released,
		this,
		&AShooterCharacter::AimingButtonReleased);

	PlayerInputComponent->BindAction("Select", IE_Pressed, this, &AShooterCharacter::SelectButtonPressed);
	PlayerInputComponent->BindAction(
		"Select",
		IE_Released,
		this,
		&AShooterCharacter::SelectButtonReleased);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AShooterCharacter::ReloadButtonPressed);
}

void AShooterCharacter::DropWeapon()
{
	if(EquippedWeapon)
	{
		FDetachmentTransformRules DetachmentTransformRules{EDetachmentRule::KeepWorld, true};
		EquippedWeapon->GetRootComponent()->DetachFromComponent(DetachmentTransformRules);

		EquippedWeapon->SetItemState(EItemState::EIS_Falling);
		EquippedWeapon->WeaponBeThrown();
		EquippedWeapon = nullptr;
	}
}

void AShooterCharacter::SelectButtonPressed()
{
	if(TraceHitItem)
	{
		TraceHitItem->StartItemCurve(this);

		if(TraceHitItem->GetPickupSound())
		{
			UGameplayStatics::PlaySound2D(this, TraceHitItem->GetPickupSound());
		}
	}
	else
	{
		DropWeapon();
	}
}

void AShooterCharacter::SelectButtonReleased()
{
}

void AShooterCharacter::SwapWeapon(AWeapon* WeaponToSwap)
{
	DropWeapon();
	EquipWeapon(WeaponToSwap);
}

void AShooterCharacter::InitializeAmmoMap()
{
	AmmoMap.Add(EAmmoType::EAT_9mm, Starting9mmAmmo);
	AmmoMap.Add(EAmmoType::EAT_AR, StartingARAmmo);
}

bool AShooterCharacter::EquippedWeaponHasAmmo()
{
	if(EquippedWeapon == nullptr) return false;

	return EquippedWeapon->GetAmmo() > 0;
}

void AShooterCharacter::PlayFireSound()
{
	/*Play fire sound*/
	if(FireSound)
	{
		UGameplayStatics::PlaySound2D(this, FireSound);
	}
}

void AShooterCharacter::SendBullet()
{
	/*Check and get Mesh of EquippedWeapon*/
	if(EquippedWeapon == nullptr) return;
	USkeletalMeshComponent* EquippedWeaponMesh = EquippedWeapon->GetItemMesh();

	/*Get BarrelSocket transform*/
	const USkeletalMeshSocket* BarrelSocket = EquippedWeaponMesh->GetSocketByName(FName("BarrelSocket"));
	if(BarrelSocket)
	{
		const FTransform BarrelSocketTransform = BarrelSocket->GetSocketTransform(EquippedWeaponMesh);

		/*Play fire FX*/
		if(MuzzleFlash)
		{
			UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, EquippedWeaponMesh, FName("BarrelSocket"));
		}

		FVector HitLocation;
		const bool bHasHit = DetermineFireHitLocation(BarrelSocketTransform.GetLocation(), HitLocation);
		
		/*Spawn fire hit particle FX*/
		if(bHasHit)
		{
			if(FireHitParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FireHitParticle, HitLocation);
			}
		}

		/*Spawn beam particle FX*/
		if(BeamParticle)
		{
			UParticleSystemComponent* BeamParticleSystemComponent =  UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				BeamParticle,
				BarrelSocketTransform);

			if(BeamParticleSystemComponent)
			{
				BeamParticleSystemComponent->SetVectorParameter(FName("Target"), HitLocation);
			}
		}
	}
}

void AShooterCharacter::PlayGunFireMontage()
{
	/*Play fire montage*/
	UAnimInstance* CharacterMeshAnimInstance = GetMesh()->GetAnimInstance();
	if(CharacterMeshAnimInstance)
	{
		if(HipFireMontage)
		{
			CharacterMeshAnimInstance->Montage_Play(HipFireMontage);
			//CharacterMeshAnimInstance->Montage_JumpToSection(FName("StartFire"));
		}
	}
}

void AShooterCharacter::ReloadButtonPressed()
{
	Reload();
}

void AShooterCharacter::Reload()
{
	if(CombatState != ECombatState::ECS_Unoccupied) return;
	if(EquippedWeapon == nullptr) return;

	/*Space left in the magazine of EquippedWeapon*/
	const int32 MagEmptySpace = EquippedWeapon->GetMagazineCapacity() - EquippedWeapon->GetAmmo();
	if(MagEmptySpace == 0) return;

	/*Do we have ammo of the correct type?*/
	if(QueryCarryingAmmo())
	{	
		CombatState = ECombatState::ECS_Reloading;
		
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if(AnimInstance && ReloadMontage)
		{
			AnimInstance->Montage_Play(ReloadMontage);
			AnimInstance->Montage_JumpToSection(EquippedWeapon->GetReloadMontageSection());
		}
	}
}

void AShooterCharacter::FinishingReloading()
{
	/*Update the Combat State*/
	CombatState = ECombatState::ECS_Unoccupied;

	if(EquippedWeapon == nullptr) return;
	const auto AmmoType{EquippedWeapon->GetAmmoType()};
	
	/*Update the AmmoMap*/
	if(AmmoMap.Contains(AmmoType))
	{
		/*Amount of ammo the Character is carrying of the EquippedWeapon type*/
		int32 CarriedAmmo = AmmoMap[AmmoType];

		/*Space left in the magazine of EquippedWeapon*/
		const int32 MagEmptySpace = EquippedWeapon->GetMagazineCapacity() - EquippedWeapon->GetAmmo();

		if(CarriedAmmo < MagEmptySpace)
		{
			/*Reload the magazine with all the ammo we are carrying*/
			EquippedWeapon->ReloadAmmo(CarriedAmmo);
			CarriedAmmo = 0;
			AmmoMap.Add(AmmoType, CarriedAmmo);
		}
		else
		{
			/*Fill the magazine*/
			EquippedWeapon->ReloadAmmo(MagEmptySpace);
			CarriedAmmo -= MagEmptySpace;
			AmmoMap.Add(AmmoType, CarriedAmmo);
		}
	}
}

bool AShooterCharacter::QueryCarryingAmmo()
{
	if(EquippedWeapon == nullptr) return false;

	auto AmmoType = EquippedWeapon->GetAmmoType();

	if(AmmoMap.Contains(AmmoType))
	{
		return AmmoMap[AmmoType] > 0;
	}

	return false;
}

void AShooterCharacter::GrabClip()
{
	if(EquippedWeapon == nullptr) return;
	if(HandSceneComponent == nullptr) return;

	/*Index for the clip bone for the EquippedWeapon*/
	int32 ClipBoneIndex{EquippedWeapon->GetItemMesh()->GetBoneIndex(EquippedWeapon->GetClipBoneName())};
	/*Store the transform of the clip*/
	ClipTransform = EquippedWeapon->GetItemMesh()->GetBoneTransform(ClipBoneIndex);

	FAttachmentTransformRules AttachmentRules{EAttachmentRule::KeepRelative, true};
	HandSceneComponent->AttachToComponent(GetMesh(), AttachmentRules, FName(TEXT("Hand_L")));
	HandSceneComponent->SetWorldTransform(ClipTransform);

	EquippedWeapon->SetMovingClip(true);
}

void AShooterCharacter::ReleaseClip()
{
	EquippedWeapon->SetMovingClip(false);
}

float AShooterCharacter::GetCrossHairSpreadMultiplier() const
{
	return CrossHairSpreadMultiplier;
}

void AShooterCharacter::IncrementOverlappedItemCount(int8 Amount)
{
	if(OverlappedItemCount + Amount <= 0)
	{
		OverlappedItemCount = 0;
		bShouldTraceForItems = false;
	}
	else
	{
		OverlappedItemCount += Amount;
		bShouldTraceForItems = true;
	}
}

FVector AShooterCharacter::GetCameraInterpLocation()
{
	const FVector CameraWorldLocation{FollowCamera->GetComponentLocation()};
	const FVector CameraForward{FollowCamera->GetForwardVector()};

	return CameraWorldLocation
		+ CameraForward * CameraInterpDistance
		+ FVector{0.f, 0.f, 1.f} * CameraInterpElevation;
}

void AShooterCharacter::GetPickupItem(AItem* Item)
{
	if(Item->GetEquipSound())
	{
		UGameplayStatics::PlaySound2D(this, Item->GetEquipSound());
	}
	
	auto Weapon = Cast<AWeapon>(Item);
	if(Weapon)
	{
		SwapWeapon(Weapon);
	}
}

void AShooterCharacter::AimingButtonPressed()
{
	bAiming = true;
}

void AShooterCharacter::AimingButtonReleased()
{
	bAiming = false;
}

void AShooterCharacter::CameraInterpZoom(float DeltaTime)
{
	if(bAiming)
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraZoomedFOV, DeltaTime, ZoomInterpSpeed);
	}
	else
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraDefaultFOV, DeltaTime, ZoomInterpSpeed);
	}
	GetFollowCamera()->SetFieldOfView(CameraCurrentFOV);
}

void AShooterCharacter::SetLookRates()
{
	if(bAiming)
	{
		BaseTurnRate = AimingTurnRate;
		BaseLookUpRate = AimingLookUpRate;
	}
	else
	{
		BaseTurnRate = HipTurnRate;
		BaseLookUpRate = HipLookUpRate;
	}
}

void AShooterCharacter::CalculateCrossHairSpread(float DeltaTime)
{
	const FVector2D WalkSpeedRange{0.f, 600.f};
	const FVector2D WalkSpeedRangeScaled{0.f, 1.f};
	FVector Velocity{GetVelocity()};
	Velocity.Z = 0.f;

	/*Factor of Velocity 2D*/
	CrossHairVelocityFactor = FMath::GetMappedRangeValueClamped(
		WalkSpeedRange,
		WalkSpeedRangeScaled,
		Velocity.Size());

	/*Factor of Being In Air*/
	if(GetCharacterMovement()->IsFalling())
	{
		/*Spread Slowly*/
		CrossHairInAirFactor = FMath::FInterpTo(CrossHairInAirFactor, 2.25f, DeltaTime, 2.25f);
	}
	else
	{
		/*Shrink Rapidly*/
		CrossHairInAirFactor = FMath::FInterpTo(CrossHairInAirFactor, 0.f, DeltaTime, 30.f);
	}

	/*Factor of Aiming*/
	if(bAiming)
	{
		CrossHairAimFactor = FMath::FInterpTo(CrossHairAimFactor, 0.6f, DeltaTime, 30.f);
	}
	else
	{
		CrossHairAimFactor = FMath::FInterpTo(CrossHairAimFactor, 0.f, DeltaTime, 30.f);
	}

	if(bFiringBullet)
	{
		CrossHairShootingFactor = FMath::FInterpTo(
			CrossHairShootingFactor,
			0.3f,
			DeltaTime,
			60.f);
	}
	else
	{
		CrossHairShootingFactor = FMath::FInterpTo(
			CrossHairShootingFactor,
			0.f,
			DeltaTime,
			60.f);
	}
	
	CrossHairSpreadMultiplier =
		0.5f
		+ CrossHairVelocityFactor
		+ CrossHairInAirFactor
		- CrossHairAimFactor
		+ CrossHairShootingFactor;
}

void AShooterCharacter::StartCrossHairBulletFire()
{
	bFiringBullet = true;

	GetWorldTimerManager().SetTimer(
		CrossHairShootTimer,
		this,
		&AShooterCharacter::FinishCrossHairBulletFire,
		ShootTimeDuration);
}

void AShooterCharacter::FinishCrossHairBulletFire()
{
	bFiringBullet = false;
}

void AShooterCharacter::FireButtonPressed()
{
	bFireButtonPressed = true;
	Fire();
}

void AShooterCharacter::FireButtonReleased()
{
	bFireButtonPressed = false;
}

void AShooterCharacter::StartFireTimer()
{
	CombatState = ECombatState::ECS_FireTimerInProgress;
	
	GetWorldTimerManager().SetTimer(
		AutoFireTimer,
		this,
		&AShooterCharacter::AutoFireReset,
		AutomaticFireRate);
}

void AShooterCharacter::AutoFireReset()
{
	CombatState = ECombatState::ECS_Unoccupied;
	if(EquippedWeaponHasAmmo())
	{
		if(bFireButtonPressed)
		{
			Fire();
		}
	}
	else
	{
		/*Reload Weapon*/
		Reload();
	}
}

bool AShooterCharacter::TraceUnderCrossHairs(FHitResult& OutHitResult, float TraceLength)
{
	/*Get Viewport Size*/
	FVector2D ViewportSize;
	if(GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}
	
	/*Get cross hair location on Screen Space*/
	FVector2D CrossHairLocation{ViewportSize.X/2.f, ViewportSize.Y/2.f};
	FVector CrossHairWorldLocation;
	FVector CrossHairWorldDirection;

	/*Transit to World Space*/
	bool bScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(
		Cast<APlayerController>(GetController()),
		CrossHairLocation,
		CrossHairWorldLocation,
		CrossHairWorldDirection);

	if(bScreenToWorld)
	{
		/*Trace from Cross Hair World Location Outward*/
		const FVector Start{CrossHairWorldLocation};
		const FVector End{CrossHairWorldLocation + CrossHairWorldDirection * TraceLength};
		GetWorld()->LineTraceSingleByChannel(
			OutHitResult,
			Start,
			End,
			ECollisionChannel::ECC_Visibility);

		/*DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, -1, 0.f, 1.f);*/

		if(OutHitResult.bBlockingHit)
		{
			return true;
		}
	}
	return false;
}

void AShooterCharacter::TraceForItems()
{
	if(bShouldTraceForItems)
	{
		FHitResult ItemTraceResult;
		TraceUnderCrossHairs(ItemTraceResult, ItemTraceLength);
		if(ItemTraceResult.bBlockingHit)
		{
			TraceHitItem = Cast<AItem>(ItemTraceResult.GetActor());
			if(TraceHitItem && TraceHitItem->GetPickupWidget())
			{
				/*Show Item's Pickup Widget*/
				TraceHitItem->GetPickupWidget()->SetVisibility(true);
			}

			/*Check to hide widget in case trace is hit*/
			if(TraceHitItemLastFrame)
			{
				if(TraceHitItem != TraceHitItemLastFrame)
				{
					TraceHitItemLastFrame->GetPickupWidget()->SetVisibility(false);
				}
			}

			/*Store a reference to HitItem for next frame*/
			TraceHitItemLastFrame = TraceHitItem;
		}
		else
		{
			if(TraceHitItemLastFrame)
			{
				/*Check to hide widget in case trace is  not hit*/
				TraceHitItemLastFrame->GetPickupWidget()->SetVisibility(false);
			}
			TraceHitItem = nullptr;
		}
	}
	else
	{
		if(TraceHitItemLastFrame)
		{
			/*Check to hide widget in case bShouldTraceForItems is true*/
			TraceHitItemLastFrame->GetPickupWidget()->SetVisibility(false);
		}
		TraceHitItem = nullptr;
	}
}

AWeapon* AShooterCharacter::SpawnDefaultWeapon()
{
	/*Check the TSubclassOf variable*/
	if(DefaultWeaponClass)
	{
		/*Spawn the Weapon*/
		return GetWorld()->SpawnActor<AWeapon>(DefaultWeaponClass);
	}
	
	return nullptr;
}

void AShooterCharacter::EquipWeapon(AWeapon* WeaponToEquip)
{
	if(WeaponToEquip)
	{
		/*GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Green, "BBB");*/
		
		/*Update Weapon's ItemState and properties*/
		WeaponToEquip->SetItemState(EItemState::EIS_Equipped);
		/*Get the Hand Socket*/
		const USkeletalMeshSocket* HandSocket = GetMesh()->GetSocketByName(FName("RightHandSocket"));

		if(HandSocket)
		{
			/*Attach the Weapon to the hand socket RightHandSocket*/
			HandSocket->AttachActor(WeaponToEquip, GetMesh());
		}

		/*Set EquippedWeapon to the newly spawned Weapon*/
		EquippedWeapon = WeaponToEquip;
	}
}

