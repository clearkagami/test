﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "UObject/NoExportTypes.h"
#include "ThirdLibInvoker.generated.h"

UCLASS()
class PI_THIRDPARTYLIBRARY_API UThirdLibInvoker : public UObject
{
	GENERATED_BODY()

	void* ExampleLibraryHandle;

public:
	void InvokeLib();
};


