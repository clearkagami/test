#include "NicknamedActor.h"
#include "NewPluginClass.h"

void ANicknamedActor::InvokeNickname()
{
	ANewPluginClass* NewPluginActor = NewObject<ANewPluginClass>();
	NewPluginActor->InvokeNewPluginClass();
}
