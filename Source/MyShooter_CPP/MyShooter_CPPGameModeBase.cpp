// Copyright Epic Games, Inc. All Rights Reserved.


#include "MyShooter_CPPGameModeBase.h"
#include "NicknamedActor.h"


void AMyShooter_CPPGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	ANicknamedActor* NicknamedActor = NewObject<ANicknamedActor>();
	NicknamedActor->InvokeNickname();
}

