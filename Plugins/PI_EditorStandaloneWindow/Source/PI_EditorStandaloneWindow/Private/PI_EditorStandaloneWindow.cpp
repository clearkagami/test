// Copyright Epic Games, Inc. All Rights Reserved.

#include "PI_EditorStandaloneWindow.h"


#include "PI_EditorStandaloneWindowStyle.h"
#include "PI_EditorStandaloneWindowCommands.h"
#include "LevelEditor.h"
#include "Widgets/Docking/SDockTab.h"
#include "Widgets/Layout/SBox.h"
#include "Widgets/Text/STextBlock.h"
#include "ToolMenus.h"
#include "Widgets/Input/SSlider.h"

#include "SMyWidget.h"

static const FName PI_EditorStandaloneWindowTabName("PI_EditorStandaloneWindow");
static const FName MyNewTab_0Name("MyNewTab_0");
static const FName MyNewTab_1Name("MyNewTab_1");
static const FName SMyWidgetName("SMyWidget");

#define LOCTEXT_NAMESPACE "FPI_EditorStandaloneWindowModule"

void FPI_EditorStandaloneWindowModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FPI_EditorStandaloneWindowStyle::Initialize();
	FPI_EditorStandaloneWindowStyle::ReloadTextures();

	FPI_EditorStandaloneWindowCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FPI_EditorStandaloneWindowCommands::Get().OpenPluginWindow,
		FExecuteAction::CreateRaw(this, &FPI_EditorStandaloneWindowModule::PluginButtonClicked),
		FCanExecuteAction());

	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FPI_EditorStandaloneWindowModule::RegisterMenus));
	
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(PI_EditorStandaloneWindowTabName, FOnSpawnTab::CreateRaw(this, &FPI_EditorStandaloneWindowModule::OnSpawnPluginTab))
		.SetDisplayName(LOCTEXT("FPI_EditorStandaloneWindowTabTitle", "PI_EditorStandalone"))
		.SetMenuType(ETabSpawnerMenuType::Hidden);

	{
		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(MyNewTab_0Name, FOnSpawnTab::CreateRaw(this, &FPI_EditorStandaloneWindowModule::OnSpawnMyNewTab_0))
			.SetDisplayName(LOCTEXT("MyNewTab_0WindowTabTitle", "MyNewTab_0"))
			.SetMenuType(ETabSpawnerMenuType::Hidden);

		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(MyNewTab_1Name, FOnSpawnTab::CreateRaw(this, &FPI_EditorStandaloneWindowModule::OnSpawnMyNewTab_1))
			.SetDisplayName(LOCTEXT("MyNewTab_1WindowTabTitle", "MyNewTab_1"))
			.SetMenuType(ETabSpawnerMenuType::Hidden);

		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(SMyWidgetName, FOnSpawnTab::CreateRaw(this, &FPI_EditorStandaloneWindowModule::OnSpawnMyWidget))
			.SetDisplayName(LOCTEXT("SMyWidgetTitle", "SMyWidget"))
			.SetMenuType(ETabSpawnerMenuType::Hidden);
	}
}

void FPI_EditorStandaloneWindowModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FPI_EditorStandaloneWindowStyle::Shutdown();

	FPI_EditorStandaloneWindowCommands::Unregister();

	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(PI_EditorStandaloneWindowTabName);
}

TSharedRef<SDockTab> FPI_EditorStandaloneWindowModule::OnSpawnPluginTab(const FSpawnTabArgs& SpawnTabArgs)
{
	FText WidgetText = FText::Format(
		LOCTEXT("WindowWidgetText", "Add code to {0} in {1} to override this window's contents"),
		FText::FromString(TEXT("FPI_EditorStandaloneWindowModule::OnSpawnPluginTab")),
		FText::FromString(TEXT("PI_EditorStandaloneWindow.cpp"))
		);

	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			// Put your tab content here!
			SNew(SBox)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text(WidgetText)
			]
		];
}

TSharedRef<SDockTab> FPI_EditorStandaloneWindowModule::OnSpawnMyNewTab_0(const FSpawnTabArgs& SpawnTabArgs)
{
	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			// Put your tab content here!
			SNew(SButton)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.OnClicked_Lambda(
				[]()
				{
					FMessageDialog::Open(EAppMsgType::Ok, FText::FromString(__FUNCTION__));
					return FReply::Handled();
				}
				)
			[
				SNew(STextBlock)
				.Text(FText::FromString("MyNewButton"))
			]
		];
}

TSharedRef<SDockTab> FPI_EditorStandaloneWindowModule::OnSpawnMyNewTab_1(const FSpawnTabArgs& SpawnTabArgs)
{
	TSharedRef<SSlider> MySlider = SNew(SSlider).Value(0.5f);

	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			// Put your tab content here!
			SNew(SVerticalBox)
			+ SVerticalBox::Slot()
			[
				MySlider
			]
			+ SVerticalBox::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			[
				SNew(STextBlock)
				.Text_Lambda(
					[MySlider]()
					{
						float SliderNum = MySlider->GetValue();
						return FText::FromString(FString::SanitizeFloat(SliderNum));
					}
					)
			]
		];
}

TSharedRef<SDockTab> FPI_EditorStandaloneWindowModule::OnSpawnMyWidget(const FSpawnTabArgs& SpawnTabArgs)
{
	return SNew(SDockTab)
		.TabRole(ETabRole::NomadTab)
		[
			// Put your tab content here!
			SNew(SMyWidget)
		];
}

void FPI_EditorStandaloneWindowModule::PluginButtonClicked()
{
	FGlobalTabmanager::Get()->TryInvokeTab(PI_EditorStandaloneWindowTabName);
	{
		FGlobalTabmanager::Get()->TryInvokeTab(MyNewTab_0Name);
		FGlobalTabmanager::Get()->TryInvokeTab(MyNewTab_1Name);
		FGlobalTabmanager::Get()->TryInvokeTab(SMyWidgetName);
	}
}

void FPI_EditorStandaloneWindowModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FPI_EditorStandaloneWindowCommands::Get().OpenPluginWindow, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar");
		{
			FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("Settings");
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FPI_EditorStandaloneWindowCommands::Get().OpenPluginWindow));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPI_EditorStandaloneWindowModule, PI_EditorStandaloneWindow)