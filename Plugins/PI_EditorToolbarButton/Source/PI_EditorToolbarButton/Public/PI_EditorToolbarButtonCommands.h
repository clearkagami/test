// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "PI_EditorToolbarButtonStyle.h"

class FPI_EditorToolbarButtonCommands : public TCommands<FPI_EditorToolbarButtonCommands>
{
public:

	FPI_EditorToolbarButtonCommands()
		: TCommands<FPI_EditorToolbarButtonCommands>(TEXT("PI_EditorToolbarButton"), NSLOCTEXT("Contexts", "PI_EditorToolbarButton", "PI_EditorToolbarButton Plugin"), NAME_None, FPI_EditorToolbarButtonStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};
