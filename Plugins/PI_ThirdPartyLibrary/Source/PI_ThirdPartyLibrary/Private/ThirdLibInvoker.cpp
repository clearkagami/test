﻿#include "ThirdLibInvoker.h"
#include "Core.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"


#include "ThirdParty/PI_ThirdPartyLibraryLibrary/ExampleLibrary.h"

	
#define LOCTEXT_NAMESPACE "UThirdLibInvoker"

void UThirdLibInvoker::InvokeLib()
{
	if(ExampleLibraryHandle == nullptr)
	{
		// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

		// Get the base directory of this plugin
		FString BaseDir = IPluginManager::Get().FindPlugin("PI_ThirdPartyLibrary")->GetBaseDir();

		// Add on the relative location of the third party dll and load it
		FString LibraryPath;
#if PLATFORM_WINDOWS
		//LibraryPath = FPaths::Combine(*BaseDir, TEXT("Binaries/ThirdParty/PI_ThirdPartyLibraryLibrary/Win64/ExampleLibrary.dll"));
		LibraryPath = FString{"D:/UrealEngineProject/UEProject4.26/MyShooter_CPP/Plugins/PI_ThirdPartyLibrary/Binaries/ThirdParty/PI_ThirdPartyLibraryLibrary/Win64/ExampleLibrary.dll"};
#elif PLATFORM_MAC
		LibraryPath = FPaths::Combine(*BaseDir, TEXT("Source/ThirdParty/PI_ThirdPartyLibraryLibrary/Mac/Release/libExampleLibrary.dylib"));
#endif // PLATFORM_WINDOWS

		ExampleLibraryHandle = !LibraryPath.IsEmpty() ? FPlatformProcess::GetDllHandle(*LibraryPath) : nullptr;
	}
	
	if (ExampleLibraryHandle)
	{
		// Call the test function in the third party library that opens a message box

		//ExampleLibraryFunction();
	}
	else
	{
		//FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("ThirdPartyLibraryError", "Failed to load example third party library!!!"));
	}
}

