﻿#pragma once
#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "NewPluginClass.generated.h"

UCLASS(Blueprintable)
class NEWPLUGINMODULE_API ANewPluginClass : public AActor
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void InvokeNewPluginClass();
};